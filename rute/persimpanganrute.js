import React, {useContext, useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';

import auth from '@react-native-firebase/auth';
import {KonteksAuth, PenyediaAuth} from '../penyedia/penyediaauth';

import RuteAkun from './ruteakun';
import RuteKonten from './rutekonten';

const PersimpanganRute = () => {
  const {pengguna, setPengguna} = useContext(KonteksAuth);
  const [ngecek, setNgecek] = useState(true); //setiap masuk app, dicek dlu kredensial si orang tsb apakah masih punya session

  const Kredensial = pengguna => {
    setPengguna(pengguna);
    if (ngecek) setNgecek(false);
  };

  useEffect(() => {
    const adaKredensial = auth().onAuthStateChanged(Kredensial);
    return adaKredensial;
  }, []);

  if (ngecek) return null;

  return (
    <PenyediaAuth>
      <NavigationContainer>
        {pengguna ? <RuteKonten /> : <RuteAkun />}
      </NavigationContainer>
    </PenyediaAuth>
  );
};

export default PersimpanganRute;
