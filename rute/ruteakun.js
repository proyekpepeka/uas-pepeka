import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import WelkamMasukJS from '../stackakun/welkammasuk';
import MasukJS from '../stackakun/masuk';
import DaftarJS from '../stackakun/daftar';

const Stack = createNativeStackNavigator();

function RuteAkun() {
  return (
    <Stack.Navigator
      initialRouteName="welkammasuk"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="welkammasuk" component={WelkamMasukJS} />
      <Stack.Screen name="masuk" component={MasukJS} />
      <Stack.Screen name="daftar" component={DaftarJS} />
    </Stack.Navigator>
  );
}

export default RuteAkun;
