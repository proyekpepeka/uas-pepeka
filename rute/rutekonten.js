import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import PengaturanJS from '../stackkonten/pengaturan';
import BerandaJS from '../stackkonten/beranda';
import WelkamJS from '../stackkonten/welkam';
import GantiUsername from '../stackkonten/gantiusername';
import GantiPass from '../stackkonten/gantipass';
import Konten from '../stackkonten/konten';
import CadanganKonten from '../stackkonten/cadangankonten';
import TentangJS from '../stackkonten/tentang';
import UnitTestingJS from '../stackkonten/unit_testing';
import EditCatatanJS from '../stackkonten/editcatatan';
import TambahCatatanJS from '../stackkonten/tambahcatatan';

const Stack = createNativeStackNavigator();

function RuteKonten() {
  return (
    <Stack.Navigator
      initialRouteName="welkam"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="welkam" component={WelkamJS} />
      <Stack.Screen name="beranda" component={BerandaJS} />
      <Stack.Screen name="pengaturan" component={PengaturanJS} />
      <Stack.Screen name="konten" component={Konten} />
      <Stack.Screen name="cadangankonten" component={CadanganKonten} />
      <Stack.Screen name="tentang" component={TentangJS} />
      <Stack.Screen name="gantiusername" component={GantiUsername} />
      <Stack.Screen name="gantipass" component={GantiPass} />
      <Stack.Screen name="tambahcatatan" component={TambahCatatanJS} />
      <Stack.Screen name="editcatatan" component={EditCatatanJS} />
      <Stack.Screen name="unit_testing" component={UnitTestingJS} />
    </Stack.Navigator>
  );
}

export default RuteKonten;
