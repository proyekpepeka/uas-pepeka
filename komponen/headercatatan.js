import React from 'react';
import {StyleSheet, View, TextInput, TouchableOpacity} from 'react-native';

const HeaderCatatan = ({teks, hint, fungsi, enkripsi}) => {
  return (
    <View style={gaya.halamanIni}>
      <TouchableOpacity>
        <TextInput
          style={{
            color: '#000',
            marginHorizontal: 10,
            fontSize: 18,
            fontWeight: 'bold',
          }}
          // multiline={true}
          numberOfLines={10}
          placeholder={hint}
          placeholderTextColor="#999"
          value={teks}
          secureTextEntry={enkripsi}
          onChangeText={teks => fungsi(teks)}
        />
      </TouchableOpacity>
    </View>
  );
};

export default HeaderCatatan;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '95%',
    height: '100%',
    marginBottom: 10,
    height: 50,
    // padding:10,
    backgroundColor: '#fafefa',
    borderRadius: 12,
    alignItems: 'center',
    flexDirection: 'row',
  },
});
