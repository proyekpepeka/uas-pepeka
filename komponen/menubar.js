import React, {useState, useEffect, useContext, createContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import {KonteksAuth} from '../penyedia/penyediaauth';
import LembarDesain from '../komponen/lembardesain';
import auth from '@react-native-firebase/auth';
import {
  Avatar,
  Title,
  Button,
  Paragraph,
  Appbar,
  Menu,
  Provider as PaperProvider,
  Card,
} from 'react-native-paper';
import TombolKotak from '../komponen/tombolkotak';
// Plane, Chase, Bounce, Wave, Wander, Pulse, Swing, Flow, Circle, CircleFade, Grid, Fold,

const MenuBar = navigation => {
  const {keluar} = useContext(KonteksAuth);
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  const email = auth().currentUser.email;
  const username = email.substring(0, email.indexOf('@'));
  return (
    <Appbar.Header dark={true}>
      <Appbar.Content title={username + ' Catat Aja'} />
      <Menu
        visible={visible}
        onDismiss={closeMenu}
        anchor={<Appbar.Action icon="dots-vertical" onPress={openMenu} />}
        contentStyle={{backgroundColor: 'black'}}>
        <Menu.Item
          onPress={() => navigation.navigate('pengaturan')}
          title="Edit Profil"
        />
        <Menu.Item
          onPress={() => navigation.navigate('tentang')}
          title="Tentang Catat Aja"
        />
        <Menu.Item onPress={() => keluar()} title="Keluar" />
      </Menu>
    </Appbar.Header>
  );
};

export default MenuBar;
