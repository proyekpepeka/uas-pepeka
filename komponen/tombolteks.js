import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

const TombolTeks = ({teks, pencet}) => {
  return (
    <TouchableOpacity>
      <Text
        style={{
          color: '#b3e5fc',
          textDecorationLine: 'underline',
        }}
        onPress={pencet}>
        {teks}
      </Text>
    </TouchableOpacity>
  );
};

export default TombolTeks;
