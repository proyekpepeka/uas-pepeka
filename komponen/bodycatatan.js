import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const BodyCatatan = ({teks, hint, fungsi, enkripsi}) => {
  return (
    <View style={gaya.halamanIni}>
      <TouchableOpacity>
        <TextInput
          style={{
            color: '#000',
            marginHorizontal: 10,
            // marginVertical: 0,
            // paddingVertical: 0,
            // height: 200,
            textAlignVertical: 'top',
          }}
          multiline={true}
          numberOfLines={100}
          placeholder={hint}
          placeholderTextColor="#999"
          value={teks}
          secureTextEntry={enkripsi}
          onChangeText={teks => fungsi(teks)}
        />
      </TouchableOpacity>
    </View>
  );
};

export default BodyCatatan;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '95%',
    marginBottom: 10,
    height: '60%',

    backgroundColor: '#fafefa',
    borderRadius: 12,
    alignItems: 'center',

    flexDirection: 'row',
  },
});
