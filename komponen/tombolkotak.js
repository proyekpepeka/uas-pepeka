import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

const TombolKotak = ({teks, warna, pencet}) => {
    return (
        <TouchableOpacity style={{
                width: '65%',
                padding: 10,
                backgroundColor: warna,
                borderRadius: 12,
                justifyContent: 'center',
                alignItems:'center',
                marginVertical:5
            }} onPress={pencet}>
                <Text style={{
                    fontSize:20,
                    fontWeight:'bold'
                }}>{teks}</Text>
            </TouchableOpacity>
    );
}

export default TombolKotak;


