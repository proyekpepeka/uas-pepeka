import React from 'react';
import {StyleSheet, View, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';


const FormTeks = ({ikon, teks, hint, fungsi, enkripsi}) => {
    return (
        <View style={gaya.halamanIni}>
            <TouchableOpacity>
            <Icon name={ikon} size={30} color="#900" style={{
                marginHorizontal:10,
            }} />
            </TouchableOpacity>
            <View style={{
                flex:1,
                borderLeftWidth:1,
                borderColor: '#555',
                marginVertical:5
            }}>
                <TextInput
                    style={{color: '#000000', marginHorizontal:10}}
                    placeholder={hint}
                    placeholderTextColor='#999'
                    value={teks}
                    secureTextEntry = {enkripsi}
                    onChangeText={(teks) => fungsi(teks)}
                />
            </View>
        </View>
    );
}

export default FormTeks;

//=============================================================

const gaya = StyleSheet.create({
    halamanIni: {
        width           : '80%',
        marginBottom    : 10,
        height          : 50,
        backgroundColor : '#fafefa',
        borderRadius    : 12,
        alignItems      : 'center',
        flexDirection   : 'row'
    },
});