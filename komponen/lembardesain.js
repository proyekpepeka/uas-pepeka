import {StyleSheet, Dimensions} from 'react-native';
const tinggi = Dimensions.get('window').height;
const lebar = Dimensions.get('window').width;

const LembarDesain = StyleSheet.create({
  viewBaris: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    // justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    // flexWrap: 'wrap',
  },
  viewrow: {
    flexDirection: 'row',
  },
  khususskrolviu: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
  },
  teksHeader: {
    fontSize: 30,
  },

  paragraf: {
    textAlign: 'justify',
    width: '75%',
    fontSize: 18,
  },

  plusnote: {
    zIndex: 2,
    position: 'absolute',
    borderRadius: 100,
    marginLeft: lebar - 60,
    marginRight: 15,
    marginTop: tinggi - 100,
    backgroundColor: '#333',
  },
  plusbutton: {
    height: 45,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LembarDesain;
