import React, {createContext, useState} from 'react';
import auth from '@react-native-firebase/auth';
import {ToastAndroid} from 'react-native';
import RuteAkun from '../rute/ruteakun';

export const KonteksAuth = createContext();

export const PenyediaAuth = ({children}) => {
  const [pengguna, setPengguna] = useState(null); //setelah default dia bakal masukin null klo ga ada apa2
  return (
    <KonteksAuth.Provider
      value={{
        pengguna, //default = auth().currentUser
        setPengguna,
        masuk: async (username, sandi) => {
          try {
            await auth().signInWithEmailAndPassword(
              username + '@catataja.com',
              sandi,
            );
            // pengguna = null;
            ToastAndroid.show('berhasil login', ToastAndroid.SHORT);
            console.log(pengguna);
          } catch (e) {
            console.log(e);
            ToastAndroid.show('Username atau Sandi salah!', ToastAndroid.SHORT);
          }
        },
        daftar: async (username, sandi) => {
          try {
            await auth().createUserWithEmailAndPassword(
              username + '@catataja.com',
              sandi,
            );
            // pengguna = null;
            ToastAndroid.show('berhasil mendaftar', ToastAndroid.SHORT);
            // await auth().signOut();
          } catch (e) {
            console.log(e);
            if (e.code == 'auth/email-already-in-use') {
              alert('Username sudah digunakan. Mohon gunakan username lain.');
            }
            if (e.code == 'auth/weak-password') {
              alert('Password harus minimal 6 karakter!');
            }
            if (e.code == 'auth/invalid-email') {
              alert(
                'Username mengandung karakter yang tidak diizinkan. Harap hanya menggunakan kombinasi angka, huruf dan _',
              );
            }
          }
        },
        keluar: async () => {
          try {
            await auth().signOut();
          } catch (e) {
            console.log(e);
          }
        },
        gantiSandi: async (passlama, passbaru, fungsi) => {
          var lama = auth.EmailAuthProvider.credential(
            auth().currentUser.email,
            passlama,
          );
          var berhasil = 'berhasil mengganti password';
          try {
            await auth()
              .currentUser.reauthenticateWithCredential(lama)
              .then(() => {
                auth()
                  .currentUser.updatePassword(passbaru)
                  .then(
                    console.log(berhasil),
                    ToastAndroid.show(berhasil, ToastAndroid.SHORT),
                  )
                  .then(fungsi);
              });
          } catch (e) {
            console.log(e);
            ToastAndroid.show('password lama anda salah!', ToastAndroid.SHORT);
          }
        },

        gantiUsername: async (sandi, usernamebaru, fungsi) => {
          var syarat = auth.EmailAuthProvider.credential(
            auth().currentUser.email,
            sandi,
          );
          var berhasil = 'berhasil mengganti username';
          try {
            await auth()
              .currentUser.reauthenticateWithCredential(syarat)
              .then(() => {
                auth()
                  .currentUser.updateEmail(usernamebaru + '@catataja.com')
                  .then(
                    console.log(berhasil),
                    ToastAndroid.show(berhasil, ToastAndroid.SHORT),
                  )
                  .then(fungsi);
              });
          } catch (e) {
            console.log(e);
            ToastAndroid.show('salah memasukkan sandi!', ToastAndroid.SHORT);
          }
        },
      }}>
      {children}
    </KonteksAuth.Provider>
  );
};
