import React, {useState, useEffect, createContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
  ScrollView,
} from 'react-native';
import {
  Avatar,
  Title,
  Button,
  Paragraph,
  Appbar,
  Menu,
  Provider as PaperProvider,
  Card,
} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import LembarDesain from '../komponen/lembardesain';
import FormTeks from '../komponen/formteks';
import TombolKotak from '../komponen/tombolkotak';
import dayjs from 'dayjs';

export const KonteksCatatan = createContext();
export const PenyediaCatatan = () => {
  const [pengguna, setPengguna] = useState(auth().currentUser.uid);
  const [catatan, setCatatan] = useState();
  const [isianjudul, setIsianJudul] = useState();
  const [isiantanggal, setIsianTanggal] = useState(JSON.stringify(dayjs()));
  const [isiankonten, setIsianKonten] = useState();

  const simpanCatatan = async () => {
    try {
      let catatan = {halo: 'yaa'};
      let catatbaru = {
        uid: `${pengguna}`,
        daftar: {
          judul: `${isianjudul}`,
          tanggal: `${isiantanggal}`,
          isi: `${isiankonten}`,
        },
      };
      let catatsekarang = await AsyncStorage.getItem('catatan');
      let simpancatatan = JSON.parse(catatsekarang);
      if (!simpancatatan) {
        simpancatatan = [];
      }
      simpancatatan.push(catatbaru);
      await AsyncStorage.setItem('catatan', JSON.stringify(simpancatatan)).then(
        () => {
          console.log('INI SIMPAN DATA');
          console.log(simpancatatan),
            ToastAndroid.show('berhasil simpan catatan', ToastAndroid.SHORT);
        },
      );
    } catch (e) {
      console.log(e);
    }
  };
  const ambilCatatan = async () => {
    try {
      let ambil = await AsyncStorage.getItem('catatan');
      ambil = JSON.parse(ambil);

      {
        ambil != null
          ? setCatatan(ambil)
          : setCatatan([{uid: 'tidak ada', daftar: []}]);
      }
      console.log('HALO');
      console.log(ambil);
      console.log('ini log berhasil ambil data');
      //   ToastAndroid.show('yuhuu berhasil ambil data', ToastAndroid.SHORT);
    } catch (e) {
      console.log(e);
    }
    return (
      <View>
        <FlatList>
          data={catatan}
          keyExtractor={({tanggal}, index) => tanggal}
          renderItem=
          {({item}) => (
            <View>
              <TouchableOpacity
                onPress={() => navigation.navigate('editcatatan')}>
                <Card style={{width: 155}}>
                  <Card.Title
                    title={item.daftar.judul}
                    subtitle={item.daftar.tanggal}
                  />
                  <Card.Content>
                    <Paragraph>{item.daftar.isi}</Paragraph>
                  </Card.Content>
                  <Card.Actions></Card.Actions>
                </Card>
              </TouchableOpacity>
            </View>
          )}
        </FlatList>
      </View>
    );
  }; // jangan lupa panggil fungsi ini di button input data
  // useEffect(() => {
  //   ambilData();
  //   return () => {};
  //   // console.log(catat);
  //   // AsyncStorage.clear();
  // }, []);
  return <KonteksCatatan.Provider value={{simpanCatatan, ambilCatatan}} />;
};

//=============================================================
