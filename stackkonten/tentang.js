import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import LembarDesain from '../komponen/lembardesain';

const TentangJS = (
  {
    /*route, navigation*/
  },
) => {
  return (
    <View style={gaya.halamanIni}>
      <Text style={LembarDesain.teksHeader}>Catat Aja Simple Notes</Text>
      <Text style={LembarDesain.paragraf}>
        {'\n'}
        {'\n'}
        {'\t'}
        {'\t'}
        {'\t'}Catat Aja adalah sebuah aplikasi catatan sederhana, ringan, namun
        powerful karena diunggulkan dengan fitur autentikasi firebase. Abadikan
        momen, catat pelajaran, tuliskan ide-ide cemerlang, bersama Catat Aja.
      </Text>
      <View
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: 'center',
        }}>
        <Text>
          {'\u00A9'} Gilang W Prasetyo{'\n'}
        </Text>
      </View>
    </View>
  );
};

export default TentangJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    // flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
