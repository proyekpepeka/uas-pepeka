import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Date} from 'react-native';
import TombolKotak from '../komponen/tombolkotak';
import auth from '@react-native-firebase/auth';
import dayjs from 'dayjs';
const PengaturanJS = ({navigation}) => {
  const [greet, setGreet] = useState();
  const email = auth().currentUser.email;
  const username = email.substring(0, email.indexOf('@'));
  const hrs = dayjs().format('H');
  const findGreet = () => {
    if (hrs === 0 || hrs < 12) setGreet('pagi');
    if (hrs === 1 || hrs < 17) setGreet('siang');
    setGreet('malam');
  };
  useEffect(() => {
    console.log(hrs);
    findGreet();
    console.log(greet);
  }, []);
  return (
    <View style={gaya.halamanIni}>
      <Text style={{fontSize: 100}}>Catat Aja!{'\n'}</Text>
      <Text style={{fontSize: 20, fontWeight: 'bold'}}>
        Selamat {greet}, <Text style={{color: '#fff59d'}}>{username}</Text>.
        {'\n'}
        Apa yang akan anda lakukan?
        {'\n'}
      </Text>
      <TombolKotak
        teks="Ganti Username"
        warna={'#ba68c8'}
        pencet={() => navigation.navigate('gantiusername')}
      />
      <TombolKotak
        teks="Ganti Sandi"
        warna={'#ba68c8'}
        pencet={() => navigation.navigate('gantipass')}
      />
      <View
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: 'center',
        }}>
        <Text>
          {'\u00A9'} Gilang W Prasetyo{'\n'}
        </Text>
      </View>
    </View>
  );
};

export default PengaturanJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
