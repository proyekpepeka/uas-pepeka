import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
  ScrollView,
} from 'react-native';
import {Card, Paragraph} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import LembarDesain from '../komponen/lembardesain';
import FormTeks from '../komponen/formteks';
import TombolKotak from '../komponen/tombolkotak';
import dayjs from 'dayjs';
import {Appbar, Provider as PaperProvider} from 'react-native-paper';
import HeaderCatatan from '../komponen/headercatatan';
import BodyCatatan from '../komponen/bodycatatan';
const TambahCatatanJS = ({navigation, route}) => {
  const [pengguna, setPengguna] = useState(auth().currentUser.uid);
  const [catatan, setCatatan] = useState();
  const [isianjudul, setIsianJudul] = useState();
  const [isiantanggal, setIsianTanggal] = useState(
    dayjs().format('dddd, DD MMMM YYYY HH:mm:ss'),
  );

  const [isiankonten, setIsianKonten] = useState();

  const simpanData = async () => {
    try {
      let catatan = {halo: 'yaa'};
      let catatbaru = {
        uid: `${pengguna}`,
        daftar: {
          judul: `${isianjudul}`,
          tanggal: `${isiantanggal}`,
          isi: `${isiankonten}`,
        },
      };
      let catatsekarang = await AsyncStorage.getItem('catatan');
      let simpancatatan = JSON.parse(catatsekarang);
      if (!simpancatatan) {
        simpancatatan = [];
      }
      simpancatatan.push(catatbaru);
      await AsyncStorage.setItem('catatan', JSON.stringify(simpancatatan)).then(
        () => {
          console.log('INI SIMPAN DATA');
          console.log(simpancatatan),
            ToastAndroid.show('berhasil simpan catatan', ToastAndroid.SHORT);
        },
      );
      // ambilData();
    } catch (e) {
      console.log(e);
    }
  };
  const ambilData = async () => {
    try {
      let ambil = await AsyncStorage.getItem('catatan');
      ambil = JSON.parse(ambil);

      {
        ambil != null
          ? setCatatan(ambil)
          : setCatatan([{uid: 'tidak ada', daftar: []}]);
      }
      console.log('HALO');
      console.log(ambil);
      console.log('ini log berhasil ambil data');
      //   ToastAndroid.show('yuhuu berhasil ambil data', ToastAndroid.SHORT);
    } catch (e) {
      console.log(e);
    }
  }; // jangan lupa panggil fungsi ini di button input data
  // useEffect(() => {
  //   ambilData();
  //   return () => {};
  //   // console.log(catat);
  //   // AsyncStorage.clear();
  // }, []);
  return (
    <PaperProvider>
      <Appbar.Header dark={true}>
        <Appbar.BackAction
          onPress={() => (
            simpanData(),
            ambilData(),
            navigation.navigate('beranda', {semua: catatan})
          )}
        />
        <Appbar.Content title="Tambah Catatan" />
      </Appbar.Header>
      <ScrollView>
        <View style={gaya.halamanIni}>
          <View
            style={{
              height: '100%',
              width: '100%',
              // justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text />
            <HeaderCatatan
              teks={isianjudul}
              hint="Judul"
              fungsi={teks => setIsianJudul(teks)}
              enkripsi={false}
            />
            <BodyCatatan
              teks={isiankonten}
              hint="Dear diary, aku mau curcol"
              fungsi={teks => setIsianKonten(teks)}
              enkripsi={false}
            />
            <TombolKotak
              teks={'tes simpan data'}
              warna="#9575cd"
              pencet={() => [simpanData()]}
            />
            <TombolKotak
              teks={'tes ambil data'}
              warna="#9575cd"
              pencet={() => ambilData()}
            />
            <TombolKotak
              teks={'hapus data'}
              warna="#9575cd"
              pencet={() => AsyncStorage.clear()}
            />
          </View>
        </View>
        <FlatList
          data={catatan}
          keyExtractor={tanggal => tanggal}
          renderItem={({item}) => (
            <View>
              <Text>{item.daftar.judul}</Text>
              <Text>{item.daftar.tanggal}</Text>
              <Text>{item.daftar.isi}</Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('editcatatan')}>
                <Card style={{width: 155}}>
                  <Card.Title
                    title={item.daftar.judul}
                    subtitle={item.daftar.tanggal}
                  />
                  <Card.Content>
                    <Paragraph>{item.daftar.isi}</Paragraph>
                  </Card.Content>
                  <Card.Actions></Card.Actions>
                </Card>
              </TouchableOpacity>
            </View>
          )}
        />
      </ScrollView>
    </PaperProvider>
  );
};

export default TambahCatatanJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
});
