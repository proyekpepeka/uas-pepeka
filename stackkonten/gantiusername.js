import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import FormTeks from '../komponen/formteks';
import {KonteksAuth} from '../penyedia/penyediaauth';
import TombolKotak from '../komponen/tombolkotak';

const GantiUsername = ({navigation}) => {
  const {pengguna, gantiUsername} = useContext(KonteksAuth);
  const [sandi, setSandi] = useState();
  const [usernamebaru, setUsernameBaru] = useState();
  const balik = () => navigation.navigate('beranda');

  return (
    <View style={gaya.halamanIni}>
      <FormTeks
        ikon={'user-alt'}
        teks={usernamebaru}
        hint="Masukkan username baru"
        fungsi={teks => setUsernameBaru(teks)}
        enkripsi={false}
      />
      <FormTeks
        ikon={'user-circle'}
        teks={sandi}
        hint="Masukkan sandi untuk melanjutkan"
        fungsi={teks => setSandi(teks)}
        enkripsi={true}
      />
      <TombolKotak
        teks={'ganti username!'}
        warna={'#ba68c8'}
        pencet={() => {
          !sandi || !usernamebaru
            ? ToastAndroid.show('kolom tidak boleh kosong!', ToastAndroid.SHORT)
            : gantiUsername(sandi, usernamebaru, balik);
        }}
      />
    </View>
  );
};

export default GantiUsername;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
