import React, {useState, useEffect, useContext, createContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Dimension,
  RefreshControl,
  SafeAreaView,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {KonteksAuth} from '../penyedia/penyediaauth';
import LembarDesain from '../komponen/lembardesain';
import auth from '@react-native-firebase/auth';
import {
  Avatar,
  Title,
  Button,
  Paragraph,
  Appbar,
  Menu,
  Provider as PaperProvider,
  Card,
} from 'react-native-paper';
import TombolKotak from '../komponen/tombolkotak';
import MenuBar from '../komponen/menubar';
import {KonteksCatatan, PenyediaCatatan} from '../penyedia/penyediacatatan';
import AsyncStorage from '@react-native-async-storage/async-storage';
import dayjs from 'dayjs';
import TombolTeks from '../komponen/tombolteks';
// Plane, Chase, Bounce, Wave, Wander, Pulse, Swing, Flow, Circle, CircleFade, Grid, Fold,

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const BerandaJS = ({navigation, route}) => {
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);
  const {keluar} = useContext(KonteksAuth);
  const [paramcatatan, setParamCatatan] = useState(route.params);
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  const email = auth().currentUser.email;
  const username = email.substring(0, email.indexOf('@'));
  const [pengguna, setPengguna] = useState(auth().currentUser.uid);
  const [catatan, setCatatan] = useState();

  // const { username } = PenyediaAuth().auth().currentUser.username;

  const ambilCatatan = async () => {
    try {
      let ambil = await AsyncStorage.getItem('catatan');
      ambil = JSON.parse(ambil);
      {
        ambil != null ? setCatatan(ambil) : setCatatan([]);
      }
      console.log(ambil);
      console.log('ini log berhasil ambil data');
    } catch (e) {
      console.log(e);
    }
  };

  const simpanCatatan = async (judul, tanggal, isi) => {
    try {
      let apanamanya = catatan;
      let UserID = auth().currentUser.uid;
      let kunci = catatan.findIndex(x => x.uid === UserID);
      let catatbaru = {
        uid: pengguna,
        daftar: {
          judul: judul,
          tanggal: tanggal,
          isi: isi,
        },
      };
      let daftarbaru = {
        judul: judul,
        tanggal: tanggal,
        isi: isi,
      };
      if (!kunci) {
        apanamanya.push(catatbaru);
      } else {
        apanamanya[kunci].daftar.push(daftarbaru);
      }
      ambilCatatan();
      ToastAndroid.show('berhasil simpan catatan', ToastAndroid.SHORT);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    ambilCatatan();

    // console.log('di bawah adalah variabel catatan');
    // console.log(catatan);
    // return () => {};
    // AsyncStorage.clear();
  }, []);
  return (
    <PaperProvider>
      <Appbar.Header dark={true}>
        <Appbar.Content title={username + ' Catat Aja'} />
        <Appbar.Action icon="restart" onPress={() => ambilCatatan()} />
        <Appbar.Action
          icon="plus"
          onPress={() => navigation.navigate('tambahcatatan')}
        />
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={<Appbar.Action icon="dots-vertical" onPress={openMenu} />}
          contentStyle={{backgroundColor: 'black'}}>
          <Menu.Item
            onPress={() => navigation.navigate('tambahcatatan')}
            title="Tambah Catatan"
          />
          <Menu.Item
            onPress={() => navigation.navigate('pengaturan')}
            title={'Profil ' + username}
          />
          <Menu.Item
            onPress={() => navigation.navigate('tentang')}
            title="Tentang Catat Aja"
          />
          <Menu.Item
            onPress={() =>
              Alert.alert(
                'Hapus seluruh data?',
                'Tindakanmu tidak dapat dibatalkan!',
                [
                  {
                    text: 'Batal',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'OK',
                    onPress: () => [
                      AsyncStorage.clear(),
                      console.log('All data wiped.'),
                    ],
                  },
                ],
              )
            }
            title="Hapus Semua Data"
          />
          <Menu.Item onPress={() => keluar()} title="Keluar" />
        </Menu>
      </Appbar.Header>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        style={{height: '100%', backgroundColor: '#303f9f'}}>
        <View style={{alignItems: 'center', height: '100%'}}>
          <View style={gaya.halamanIni}>
            <Text>
              Welkam, {username}
              {'\n'}
            </Text>
          </View>
          <FlatList
            // horizontal
            contentContainerStyle={gaya.list}
            data={catatan}
            keyExtractor={item => item.tanggal}
            renderItem={({item}) => (
              <View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('editcatatan')}>
                  <Card
                    style={{
                      width: 350,
                      margin: 10,
                      height: 150,
                    }}>
                    <Card.Title
                      title={item.daftar.judul}
                      subtitle={item.daftar.tanggal}
                    />
                    <Card.Content>
                      <Paragraph>{item.daftar.isi}</Paragraph>
                    </Card.Content>
                    <Card.Actions></Card.Actions>
                  </Card>
                </TouchableOpacity>
              </View>
            )}
          />
          {/* <TombolKotak
            warna="red"
            teks="unit testing"
            pencet={() =>
              navigation.navigate('unit_testing', {
                // semua: catatan,
                // simpan: (judul, tanggal, isi) =>
                //   simpanCatatan(judul, tanggal, isi),
              })
            }
          />
          <TombolKotak
            warna="green"
            teks="console log data"
            pencet={() =>
              // console.log(catatan.findIndex(auth().currentUser.uid))
              console.log(catatan)
            }
          /> */}
        </View>
      </ScrollView>
    </PaperProvider>
  );
};

export default BerandaJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    backgroundColor: '#303f9f',
    // justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    // height: '100%',
  },
  list: {
    backgroundColor: '#303f9f',
    flexDirection: 'column-reverse',
    justifyContent: 'flex-end',
    // height: '100%',
  },
});

//beranda

//https://react-native-async-storage.github.io/async-storage/docs/install/
//yarn add @react-native-async-storage/async-storage
//import AsyncStorage from '@react-native-async-storage/async-storage';
// tulis aja asyncstorange
