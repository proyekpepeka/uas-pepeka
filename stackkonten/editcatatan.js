import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import BodyCatatan from '../komponen/bodycatatan';
import HeaderCatatan from '../komponen/headercatatan';
import MenuBar from '../komponen/menubar';
import {
  Avatar,
  Title,
  Button,
  Paragraph,
  Appbar,
  Menu,
  Provider as PaperProvider,
  Card,
} from 'react-native-paper';

const EditCatatanJS = () => {
  const [catatan, setCatatan] = useState();
  const [tanggalcatatan, setTanggalCatatan] = useState();
  const editCatatan = async () => {
    try {
      let datalama = await AsyncStorage.getItem('catatan');
      ambil = JSON.parse(ambil);
      {
        ambil != null ? setCatatan(ambil) : setCatatan([]);
      }
      console.log(ambil);
      console.log('ini log berhasil ambil data');
    } catch (e) {
      console.log(e);
    }
  };

  const ambilSatuCatatan = async () => {
    let filter = catatan.findIndex(x => x.tanggal === tanggalcatatan);
  };
  return (
    <PaperProvider>
      <Appbar.Header dark={true}>
        <Appbar.Content title={username + ' Catat Aja'} />
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={<Appbar.Action icon="dots-vertical" onPress={openMenu} />}
          contentStyle={{backgroundColor: 'black'}}>
          <Menu.Item
            onPress={() => navigation.navigate('tambahcatatan')}
            title="Tambah Catatan"
          />
          <Menu.Item
            onPress={() => navigation.navigate('pengaturan')}
            title="Edit Profil"
          />
          <Menu.Item
            onPress={() => navigation.navigate('tentang')}
            title="Tentang Catat Aja"
          />
          <Menu.Item onPress={() => keluar()} title="Keluar" />
        </Menu>
      </Appbar.Header>
      <View style={gaya.halamanIni}>
        <Text></Text>
        <HeaderCatatan hint="ini judul" />
        <BodyCatatan hint="ini isi" />
      </View>
    </PaperProvider>
  );
};

export default EditCatatanJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});
