import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import TombolKotak from '../komponen/tombolkotak';
import FormTeks from '../komponen/formteks';

const CadanganKonten = ({navigation}) => {
  const [pengguna, setPengguna] = useState(auth().currentUser.uid);
  const [catat, setCatat] = useState();
  const [judul, setJudul] = useState();
  const [tanggal, setTanggal] = useState();
  const [isi, setIsi] = useState();

  useEffect(() => {
    console.log(pengguna);
    ambilData();
    // AsyncStorage.clear();
  }, []);

  simpanData = async () => {
    try {
      let catataja = [];
      let daftar = catataja.push({
        uid: pengguna,
        daftar: [
          {
            judul: judul,
            tanggal: tanggal,
            isi: isi,
            key: judul + tanggal,
          },
        ],
      });
      await AsyncStorage.setItem(
        '@catataja',
        JSON.stringify(
          catataja,
          ToastAndroid.show('berhasil simpan catatan', ToastAndroid.SHORT),
        ),
      );
    } catch (e) {
      console.log(e);
    }
  }; // jangan lupa panggil fungsi ini di button input data

  ambilData = async () => {
    try {
      let ambil = await AsyncStorage.getItem('@catataja');
      ambil = JSON.parse(ambil);

      {
        ambil != null
          ? setCatat(ambil)
          : setCatat([{uid: 'tidak ada', daftar: []}]);
      }
      console.log('ini log berhasil ambil data');
      //   ToastAndroid.show('yuhuu berhasil ambil data', ToastAndroid.SHORT);
    } catch (e) {
      console.log(e);
    }
  }; // jangan lupa panggil fungsi ini di component did mount (cdm)

  return (
    <View style={gaya.halamanIni}>
      <FormTeks
        ikon="heading"
        teks={judul}
        hint="Judul Catatan"
        fungsi={teks => setJudul(teks)}
        enkripsi={false}
      />
      <FormTeks
        ikon="table"
        teks={tanggal}
        hint="Tanggal"
        fungsi={teks => setTanggal(teks)}
        enkripsi={false}
      />
      <FormTeks
        teks={isi}
        hint="Dear diary, aku mau curhat ..."
        fungsi={teks => setIsi(teks)}
        enkripsi={false}
      />
      <TombolKotak
        teks="balik su"
        warna="#8d6e63"
        pencet={() => navigation.goBack()}
      />
      <TombolKotak
        teks="Simpan Catatan"
        warna="#2196f3"
        pencet={() => [simpanData(), console.log(catat)]}
      />
      <TombolKotak
        warna="#8d6e63"
        teks="demi console.log"
        pencet={() => [console.log(catat[0].daftar[0].isi), console.log(catat)]}
      />
    </View>
  );
};

export default CadanganKonten;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
