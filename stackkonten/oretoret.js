import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import BodyCatatan from '../komponen/bodycatatan';
import HeaderCatatan from '../komponen/headercatatan';
import {KonteksAuth} from '../penyedia/penyediaauth';
import auth from '@react-native-firebase/auth';
import dayjs from 'dayjs';

import MenuBar from '../komponen/menubar';
import {
  Avatar,
  Title,
  Button,
  Paragraph,
  Appbar,
  Menu,
  Provider as PaperProvider,
  Card,
} from 'react-native-paper';

import {ToastAndroid, FlatList, ScrollView} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const TambahCatatanJS = ({navigation}) => {
  const [pengguna, setPengguna] = useState(auth().currentUser.uid);
  const [catatan, setCatatan] = useState();
  const [isianjudul, setIsianJudul] = useState();
  const [isiankonten, setIsianKonten] = useState();
  const [isiantanggal, setIsianTanggal] = useState(
    JSON.stringify(dayjs().format('YYYY-MM-DDTHH:mm:ss.sssZ')),
  );
  const simpanData = async () => {
    try {
      // let catatan = {halo: 'yaa'};
      let catatbaru = {
        uid: `${pengguna}`,
        daftar: {
          judul: `${isianjudul}`,
          tanggal: `${isiantanggal}`,
          isi: `${isiankonten}`,
        },
      };
      let catatsekarang = await AsyncStorage.getItem('catatan');
      let simpancatatan = JSON.parse(catatsekarang);
      if (!simpancatatan) {
        simpancatatan = [];
      }
      simpancatatan.push(catatbaru);
      await AsyncStorage.setItem('catatan', JSON.stringify(simpancatatan)).then(
        () => {
          console.log('INI SIMPAN DATA');
          console.log(simpancatatan),
            ToastAndroid.show(
              'berhasil menyimpan catatan baru',
              ToastAndroid.SHORT,
            );
        },
      );
    } catch (e) {
      console.log(e);
    }
  };
  const ambilData = async () => {
    try {
      let ambil = await AsyncStorage.getItem('catatan');
      ambil = JSON.parse(ambil);

      {
        ambil != null
          ? setCatatan(ambil)
          : setCatatan([{uid: 'tidak ada', daftar: []}]);
      }
      console.log('HALO');
      console.log(ambil);
      console.log('ini log berhasil ambil data');
      //   ToastAndroid.show('yuhuu berhasil ambil data', ToastAndroid.SHORT);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <PaperProvider>
      <Appbar.Header dark={true}>
        <Appbar.BackAction
          onPress={() => (navigation.navigate('beranda'), simpanData())}
        />
        <Appbar.Content title="Tambah Catatan" />
      </Appbar.Header>
      <View style={gaya.halamanIni}>
        <Text></Text>
        <HeaderCatatan
          teks={isianjudul}
          hint="Masukkan Judul "
          fungsi={teks => setIsianJudul(teks)}
          enkripsi={false}
        />
        <BodyCatatan
          teks={isiankonten}
          hint="ini isi catatanmu"
          fungsi={teks => setIsianKonten(teks)}
          enkripsi={false}
        />
      </View>
    </PaperProvider>
  );
};

export default TambahCatatanJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});
