import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import FormTeks from '../komponen/formteks';
import {KonteksAuth} from '../penyedia/penyediaauth';
import TombolKotak from '../komponen/tombolkotak';

const GantiPass = ({navigation}) => {
  const {pengguna, gantiSandi} = useContext(KonteksAuth);
  const [sandilama, setSandiLama] = useState();
  const [sandibaru, setSandiBaru] = useState();
  const balik = () => navigation.goBack();

  return (
    <View style={gaya.halamanIni}>
      <FormTeks
        ikon={'lock-open'}
        teks={sandilama}
        hint="Masukkan sandi lama"
        fungsi={teks => setSandiLama(teks)}
        enkripsi={true}
      />
      <FormTeks
        ikon={'lock'}
        teks={sandibaru}
        hint="Masukkan sandi baru"
        fungsi={teks => setSandiBaru(teks)}
        enkripsi={true}
      />
      <TombolKotak
        teks={'ganti password!'}
        warna={'#ba68c8'}
        pencet={() => {
          !sandilama || !sandibaru
            ? ToastAndroid.show('kolom tidak boleh kosong!', ToastAndroid.SHORT)
            : gantiSandi(sandilama, sandibaru, balik);
        }}
      />
    </View>
  );
};

export default GantiPass;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
