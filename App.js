import React from 'react';
import {PenyediaAuth} from './penyedia/penyediaauth';
import PersimpanganRute from './rute/persimpanganrute';

const App = () => {
  return (
    <PenyediaAuth>
      <PersimpanganRute />
    </PenyediaAuth>
  );
};

export default App;

// //https://reactnavigation.org/docs/getting-started
// // yarn add @react-navigation/native react-native-screens react-native-safe-area-context @react-navigation/native-stack
// // Taruh ini di android/app/src/main/java/mainActivity.java
// import android.os.Bundle;
// @Override protected void onCreate(Bundle savedInstanceState) {super.onCreate(null);}
// //import {createNativeStackNavigator} from '@react-navigation/native-stack';

// //const Stack = createNativeStackNavigator();

// //https://github.com/tienphaw/react-native-animated-spinkit
// //yarn add react-native-animated-spinkit
// //import { Grid } from 'react-native-animated-spinkit'
// // Plane, Chase, Bounce, Wave, Wander, Pulse, Swing, Flow, Circle, CircleFade, Grid, Fold,
// //<Grid size={75} color="#FFF" style={{margin: 50}} />

// //https://github.com/oblador/react-native-vector-icons
// //yarn add react-native-vector-icons
// //Edit android/app/build.gradle ( NOT android/build.gradle ) and add the following:
// // apply from: "../../node_modules/react-native-vector-icons/fonts.gradle"
// //import Icon from 'react-native-vector-icons/FontAwesome5';
// //<Icon name="rocket" size={30} color="#900" />

// //https://github.com/saleel/react-native-super-grid
// //yarn add react-native-super-grid
// //import { FlatGrid } from 'react-native-super-grid';
// //ketik aja grid

// //rnyoutubeplayer
// //rnsnackbar
// //https://github.com/calintamas/react-native-toast-message/blob/main/docs/quick-start.md
// //yarn add react-native-toast-message
// //import Toast from 'react-native-toast-message';
// //ketik aja toastmessage
// //jangan lupa taruh <Toast /> di hierarki terakhir

// //rnlineargradient
// //https://github.com/react-native-modal/react-native-modal
// //yarn add react-native-modal
// //import Modal from 'react-native-modal';
// //ketik aja modal
// //nulisnya di hierarki bebas

// //https://github.com/henninghall/react-native-date-picker
// //yarn add react-native-date-picker
// //import DatePicker from 'react-native-date-picker';
// //ketik aja datepicker
// //nulisnya di hierarki sesuai kebutuhan, atau bisa di dalem modal

// //https://reactnavigation.org/docs/bottom-tab-navigator
// //yarn add @react-navigation/bottom-tabs
// //import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// //const Tabs = createBottomTabNavigator();

// //const TabsBawah = () => (
// //	<Tabs.Navigator>
// //		<Tabs.Screen name="Homescreen" component={Home} />
// //	</Tabs.Navigator>
// //);

// //https://react-native-async-storage.github.io/async-storage/docs/install/
// //yarn add @react-native-async-storage/async-storage
// //import AsyncStorage from '@react-native-async-storage/async-storage';
// // tulis aja asyncstorange

//https://rnfirebase.io
//yarn add @react-native-firebase/app @react-native-firebase/analytics @react-native-firebase/auth
//buat projek baru di firebase google,
// isi nama package sesuai yang di /android/app/src/main/AndroidManifest.xml
// download file google-services.json, taruh di android/app
//tambahin ini di android/build.gradle -- buildscript{dependencies{.....nah disini
// classpath 'com.google.gms:google-services:4.3.14'
//tambahin ini di android/app/build.gradle
// apply plugin: 'com.android.application'
// apply plugin: 'com.google.gms.google-services'
//rebuild aplikasi, biar autolinking? gausah, nanti aja pas yarn android bakal otomatis
//kalo mau disable google analytics, set true ke false di :
//C:/Users/ibnab/loginsample/android/app/build/intermediates/merged_manifest/debug/AndroidManifest.xml
//firebase_analytics_collection_deactivated (baris 50an) set ke true. ini disable permanen.
