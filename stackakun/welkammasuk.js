import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {StackActions} from '@react-navigation/native';
import {Grid} from 'react-native-animated-spinkit';
//https://github.com/tienphaw/react-native-animated-spinkit
//yarn add react-native-animated-spinkit
//import { Grid } from 'react-native-animated-spinkit'
// Plane, Chase, Bounce, Wave, Wander, Pulse, Swing, Flow, Circle, CircleFade, Grid, Fold,

const WelkamMasukJS = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.dispatch(StackActions.replace('masuk'));
    }, 1500);
  });

  return (
    <View style={gaya.halamanIni}>
      <Grid size={75} color="#FFF" style={{margin: 50}} />
      <Text>Selamat datang. Anda akan {'\n'}diarahkan ke halaman login.</Text>
    </View>
  );
};

export default WelkamMasukJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#333',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
