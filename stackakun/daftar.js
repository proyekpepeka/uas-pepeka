import React, {useState, useContext} from 'react';
import {StyleSheet, View, Text, ToastAndroid} from 'react-native';

import FormTeks from '../komponen/formteks';
import TombolKotak from '../komponen/tombolkotak';
import TombolTeks from '../komponen/tombolteks';
import LembarDesain from '../komponen/lembardesain';
import {KonteksAuth} from '../penyedia/penyediaauth';

const DaftarJS = ({navigation}) => {
  const [username, setUsername] = useState();
  const [namalengkap, setNamaLengkap] = useState();
  const [sandi, setSandi] = useState();
  const {daftar} = useContext(KonteksAuth);

  return (
    <View style={gaya.halamanIni}>
      <FormTeks
        ikon="user"
        teks={username}
        hint="Username"
        fungsi={teks => setUsername(teks)}
        enkripsi={false}
      />
      {/* <FormTeks
        ikon="id-card"
        teks={namalengkap}
        hint="Nama Lengkap"
        fungsi={teks => setNamaLengkap(teks)}
        enkripsi={false}
      /> */}
      <FormTeks
        ikon="lock"
        teks={sandi}
        hint="Kata Sandi"
        fungsi={teks => setSandi(teks)}
        enkripsi={true}
      />

      <TombolKotak
        teks="daftar"
        warna="#009688"
        pencet={() => {
          !username || !sandi
            ? ToastAndroid.show('kolom tidak boleh kosong!', ToastAndroid.SHORT)
            : daftar(username, sandi);
        }}
      />
      <Text></Text>
      <View style={LembarDesain.viewrow}>
        <Text>Sudah memiliki akun? </Text>
        <TombolTeks teks="login di sini!" pencet={() => navigation.goBack()} />
      </View>
    </View>
  );
};

export default DaftarJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
