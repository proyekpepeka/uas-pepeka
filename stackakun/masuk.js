import React, {useState, useContext} from 'react';
import {StyleSheet, View, Text, ToastAndroid} from 'react-native';

import TombolKotak from '../komponen/tombolkotak';
import FormTeks from '../komponen/formteks';
import {KonteksAuth} from '../penyedia/penyediaauth';

const MasukJS = ({navigation}) => {
  const [pengguna, setPengguna] = useState();
  const [sandi, setSandi] = useState();

  const {masuk} = useContext(KonteksAuth);

  return (
    <View style={gaya.halamanIni}>
      <FormTeks
        ikon="user"
        teks={pengguna}
        hint="Username"
        fungsi={teks => setPengguna(teks)}
        enkripsi={false}
      />
      <FormTeks
        ikon="lock"
        teks={sandi}
        hint="Password"
        fungsi={teks => setSandi(teks)}
        enkripsi={true}
      />

      <TombolKotak
        teks="login"
        warna="#ba68c8"
        pencet={() => {
          !pengguna || !sandi
            ? ToastAndroid.show('kolom tidak boleh kosong!', ToastAndroid.SHORT)
            : //  (!sandi ?
              //   ToastAndroid.show('sandi tidak boleh kosong!', ToastAndroid.SHORT) :
              masuk(pengguna, sandi);
        }}
      />
      <TombolKotak
        teks="daftar"
        warna="#009688"
        pencet={() => navigation.navigate('daftar')}
      />
    </View>
  );
};

export default MasukJS;

//=============================================================

const gaya = StyleSheet.create({
  halamanIni: {
    width: '100%',
    height: '100%',
    backgroundColor: '#303f9f',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
