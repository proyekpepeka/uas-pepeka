let coba = [
  {
    uid: 123456789,
    data: [
      {
        judul: 'ayam',
        create: 'Des 22 2022 9:22:56',
        lastEdit: 'Des 22 2022 9:25:33',
        kategori: 'uncategorized',
        konten:
          'Pada suatu hari yang cerah, ' +
          '\b seekor ayam pergi ke sawah mencari makan.' +
          '\b Seketika matanya tertuju pada ulat gemuk yang sedang ' +
          '\b berjalan di atas daun. Ayam pun melangkahkan kaki, ' +
          '\b mengendapkan langkah. Happ, tertangkap! ' +
          '\b Petani berhasil menangkap si ayam, dan langsung ' +
          '\b menggorok gelambir jakun si ayam. Darah mengucur. ' +
          '\b "fyuhh... Menu hari ini sudah ditentukan, Opor" gumam ' +
          '\b si petani, "sekarang tinggal menyiapkan nasi..." ',
      },
      {
        judul: 'sapi',
        create: 'Des 20 2022 23:24:41',
        lastEdit: 'Des 21 2022 04:13:23',
        kategori: 'peri-kemanusiaan-dan-kesapian',
        konten:
          'Pada suatu hari yang cerah, ' +
          '\b seekor sapi pergi ke bukit mencari udara.' +
          '\b Seketika matanya tertuju pada burung merpati yang sedang ' +
          '\b melayang di angkasa. kepakan sayap yang indah, ' +
          '\b aliran udara yang penuh kebebasan! dan tentu saja ' +
          '\b membuat semua makhluk iri. kecuali si sapi. yup, dia ' +
          '\b sudah meluncur sejak tadi, membuktikan teori ' +
          '\b percepatan gravitasi. empat detik yang penuh nuansa, ' +
          '\b satu detik untuk hukum aksi = reaksi. lukisan darah. ',
      },
    ],
  },
  {
    uid: 000000001,
    data: [
      {
        judul: 'panci',
        create: 'Des 25 2022 4:14:52',
        lastEdit: 'Des 27 2022 8:25:14',
        kategori: 'uncategorized',
        konten:
          'Kalian mau tahu, apa yang terjadi kalau ' +
          '\b sebuah panci terjun dari ketinggian 330.000 kaki?' +
          '\b Yup, Jeko dan Smith, dua ilmuwan terkemuka ini akan ' +
          '\b membuktikannya. Eksperimen. ' +
          '\b dua hari lalu mereka berusaha membuat kontrak dengan ' +
          '\b spaceX untuk mengikutkan panci mereka dalam ekspedisi ke ' +
          '\b luar angkasa. mereka menghabiskan beberapa ribu dolar ' +
          '\b pada eksperimen kali ini. setelah mereka mempersiapkan ' +
          '\b segalanya, perizinan, kamera, koreografer, helikopter, ' +
          '\b hingga mencuci panci guna sterilisasi eksperimen, ' +
          '\b akhirnya roket berangkat. ya sudah roketnya saja yang ' +
          '\b berangkat. Siapa bilang spaceX akan mengikutkan panci ' +
          '\b bermasalah ini ke dalam ekspedisi mereka. ',
      },
    ],
  },
];

//=========================================================

let anggapIniUIDfirebase = 123456789;

let uji = coba.findIndex(x => x.uid === anggapIniUIDfirebase);

// console.log(coba[0].data[0].konten); //menampilkan note berjudul "ayam"
// console.log(uji); //menampilkan indeks data milik user dengan uid 123456789
console.log(coba[uji]); //menampilkan data milik user dengan uid 123456789
